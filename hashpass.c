#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    // Check to see if user supplied 2 arguments
    if (argc < 2 || argc > 3)
    {
        printf("Usage: ./hashpass input_file output_file\n");
        exit(2);
    }
    
    // Open first argument for reading
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    FILE *fpout;
    // Open second argument for writing
    if (argc == 3)
    {
        fpout = fopen(argv[2], "w");
        if (!fpout)
        {
            printf("Can't open %s for writing\n", argv[2]);
            exit(1);
        }
    }
    
    // Read all lines and print them as we go
    char line[100];
    while(fgets(line, 100, fp) != NULL)
    {
        char *temp = md5(line, strlen(line)-1);
        
        if (argc == 3)
            fprintf(fpout, "%s\n", temp);
        else
            printf("%s\n", temp);
    }
    
    // Close file
    fclose(fp);
    if (argc == 3) fclose(fpout);
}